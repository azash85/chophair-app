angular.module('controllers', [])

.controller('LoginCtrl', function ($scope, $state, $http, baseUrl, $rootScope, $localStorage, $ionicLoading) {
    $scope.login = function (email, password) {
        $scope.login_error = '';
        $ionicLoading.show({
            template: 'Loading...'
        });
        $http({
            url: baseUrl + 'mobile/login',
            method: 'POST',
            params: { email: email, password: password }
        }).success(function (result) {
            $localStorage.access_token = result.access_token;
            $state.go('tab.account');
        }).error(function (error) {
            $scope.login_error = error.message;
        }).finally(function () {
            $ionicLoading.hide();
        });
    }
})

.controller('AccountCtrl', function ($scope, $state, $http, baseUrl, $rootScope, $localStorage, $ionicLoading) {

    function getMain() {
        $ionicLoading.show({
            template: 'Loading...'
        });
        $http({
            url: baseUrl + 'mobile/main/get',
            method: 'POST',
            params: { access_token: $localStorage.access_token }
        }).success(function (result) {
            $scope.main = result['client'];
            console.log($scope.main);
        }).error(function (error) {
            console.log(error);
        }).finally(function(){
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        });
    }
    getMain();

    $scope.doRefresh = function () {
        getMain();
    }

    $scope.setShopState = function () {
        $scope.main['is_open'] ^= true;
        console.log($scope.main['is_open']);
        $ionicLoading.show({
            template: 'Loading...'
        });
        $http({
            url: baseUrl + 'mobile/shop/is_open',
            method: 'POST',
            params: { access_token: $localStorage.access_token, is_open: $scope.main['is_open']  }
        }).success(function (result) {
            //callback
        }).error(function (error) {
            console.log(error);
        }).finally(function () {
            $ionicLoading.hide();
        });
    }
})

.controller('AccountDetailCtrl', function ($scope, $stateParams, $http, baseUrl, $localStorage, $ionicLoading, $state) {
    $scope.account_type = $stateParams.accountType;
    if ($scope.account_type == 'bookings') {
        $scope.bookings_tab = true;
        $ionicLoading.show({
            template: 'Loading...'
        });
        $http({
            url: baseUrl + 'mobile/bookings/get',
            method: 'POST',
            params: {access_token: $localStorage.access_token}
        }).success(function (result) {
            console.log(result);
            $scope.bookings = result.bookings
        }).error(function (error) {
            console.log(error);
        }).finally(function () {
            $ionicLoading.hide();
        });
    }
    if ($scope.account_type == 'account') {
        $scope.account_tab = true;
        $ionicLoading.show({
            template: 'Loading...'
        });
        $http({
            url: baseUrl + 'mobile/account/get',
            method: 'POST',
            params: { access_token: $localStorage.access_token }
        }).success(function (result) {
            $scope.account = result['client'];
            console.log(result);
        }).error(function (error) {
            console.log(error);
        }).finally(function () {
            $ionicLoading.hide();
        });

        $scope.saveAccountInfo = function (account) {
            console.log(account);
            $ionicLoading.show({
                template: 'Loading...'
            });
            $http({
                url: baseUrl + 'mobile/account/post',
                method: 'POST',
                params: { access_token: $localStorage.access_token, account_info: account }
            }).success(function (result) {
                console.log(result);
            }).error(function (error) {
                console.log(error);
            }).finally(function () {
                $ionicLoading.hide();
            });
        }
    }
    if ($scope.account_type == 'open') {
        $scope.opening_hours = {};
        $scope.open_tab = true;

        $ionicLoading.show({
            template: 'Loading...'
        });
        $http({
            url: baseUrl + 'mobile/opening_hours/get',
            method: 'POST',
            params: { access_token: $localStorage.access_token }
        }).success(function (result) {
            angular.forEach(result.opening_hours, function (value, key) {
                var open = value.substring(0, 5);
                var close = value.substring(8, 13);
                if (value == 'Closed') {
                    $scope.opening_hours[key] = { open: '', close: ''};
                }
                else {
                    $scope.opening_hours[key] = { open: open, close: close };
                }
            });
        }).error(function (error) {
            console.log(error);
        }).finally(function () {
            $ionicLoading.hide();
        });

        function createTimeRange() {
            var range = [];
            for (var h = 0; h < 24; h++) {
                var hour = h;
                for (var m = 0; m < 46; m += 15) {
                    var minute = m;
                    if (h < 10) {
                        hour = '0' + h;
                    }
                    if (m == 0) {
                        minute = '0' + m;
                    }
                    range.push(hour+':'+minute);
                }
            }
            return range;
        }
        $scope.timeRange = createTimeRange();

        $scope.checkboxOpenClose = function (time) {
            if (time.open) {
                this.time.open = '';
                this.time.close = '';
                console.log('closed');
            }
            else {
                this.time.open = '09:00';
                this.time.close = '16:00';
                console.log('09:00');
            }
            console.log($scope.opening_hours);

        }

        $scope.saveOpeningTimes = function (opening_hours) {
            console.log(opening_hours);
            opening_hours = convertOpeningHours(opening_hours);
            console.log(opening_hours);

            $ionicLoading.show({
                template: 'Loading...'
            });
            $http({
                url: baseUrl + 'mobile/opening_hours/post',
                method: 'POST',
                params: { access_token: $localStorage.access_token, opening_hours: opening_hours }
            }).success(function (result) {
                console.log(result);
            }).error(function (error) {
                console.log(error);
            }).finally(function () {
                $ionicLoading.hide();
            });
        }
        function convertOpeningHours(opening_hours) {
            var result = {};
            angular.forEach(opening_hours, function (value, key) {
                if (value.open || value.close) {
                    result[key] = value.open + ' - ' + value.close;
                }
                else {
                    result[key] = 'Closed';
                }
            });
            return result;
        }

    }
    if ($scope.account_type == 'prices') {
        $scope.prices_tab = true;
        $ionicLoading.show({
            template: 'Loading...'
        });
        $http({
            url: baseUrl + 'mobile/prices/get',
            method: 'POST',
            params: { access_token: $localStorage.access_token }
        }).success(function (result) {
            console.log(result);
            $scope.prices = result.services_prices;
        }).error(function (error) {
            console.log(error);
        }).finally(function () {
            $ionicLoading.hide();
        });
    }

    $scope.cancel = function () {
        $state.go('tab.account');
    }
    
})

.controller('MenuController', function ($scope, $state, $window) {
    $scope.menu_state = $window.location.hash.indexOf("login") > -1;
    console.log($scope.menu_state);
    console.log($window.location.hash);
});
